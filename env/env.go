package env

import (
	"log"

	"github.com/facebookgo/flagenv"
	"github.com/joho/godotenv"
)

//Env interface
type Env interface {
	Parse()
}

type myEnv struct{}

//NewEnv fun
func NewEnv() Env {
	return &myEnv{}
}

func (m *myEnv) Parse() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
	flagenv.Parse()
}
